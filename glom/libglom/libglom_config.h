/* glom/libglom/libglom_config.h.  Generated from libglom_config.h.in by configure.  */

#ifndef GLOM_LIBGLOM_CONFIG_H
#define GLOM_LIBGLOM_CONFIG_H

/* "definition of GLOM_DTD_INSTALL_DIR" */
/* #undef GLOM_DTD_INSTALL_DIR */

/* Define to 1 if you have the `dcgettext' function. */
#define HAVE_DCGETTEXT 1

/* Define to 1 if you have the <dlfcn.h> header file. */
#define HAVE_DLFCN_H 1

/* Define if the GNU gettext() function is already present or preinstalled. */
#define HAVE_GETTEXT 1

/* Define to 1 if you have the `strptime' function. */
#define HAVE_STRPTIME 1

/* Define to the installation prefix of the iso-codes module. */
#define ISO_CODES_PREFIX "/usr"

#endif /* GLOM_LIBGLOM_CONFIG_H */
